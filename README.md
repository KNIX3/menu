# menu

# Overview
A generic back end for creating menu programs for things such as bookmarks, music, files, etc.

# Usage

```
Usage:
   -h, --help                           Display this message.
   -i, --init [path] [handler]          Initialize a new store.
   -a, --add [path] [item-name]         Add an item to the store.
   -r, --remove [path] [item-name]      Remove an item from the store.
   -l, --list [path]                    List all items in the store.
   -o, --output [path] [item-name]      Output the parsed version of an item in the store.
   -e, --edit [path] [item-name]        Edit an item in the store.
```

# Downloads
Currently, there are no official release of `menu`.

* You can download and install it from the AUR if you are running an arch system via [menu-git](https://aur.archlinux.org/packages/menu-git/).
* The only other way to install menu is to download the source located at [GitLab](https://gitlab.com/KNIX3/menu/).

# Dependencies
* A POSIX shell
